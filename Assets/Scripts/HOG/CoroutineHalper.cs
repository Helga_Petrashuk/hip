﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CoroutineHalper
{
    public static void DalayCall(MonoBehaviour obj, System.Action action, float delay)
    {
        obj.StartCoroutine(Delay(delay, action));
    }

    private static IEnumerator Delay (float time, System.Action action)
    {
        yield return new WaitForSeconds(time);

        action?.Invoke();

    }
}
