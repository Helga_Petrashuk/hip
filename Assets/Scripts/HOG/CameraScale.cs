﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScale : MonoBehaviour
{
    [SerializeField] private Vector2 _referencePozition;
   
    void Awake()
    {
        float referenceAspekt = _referencePozition.x / _referencePozition.y;
        float screeanAspekt = Camera.main.aspect;
        Camera.main.orthographicSize *= (referenceAspekt / screeanAspekt);
    }

}
