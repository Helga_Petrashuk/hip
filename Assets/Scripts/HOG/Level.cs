﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    private List <Item> _gameItems = new List <Item>();
    private Dictionary<string, ItemData> _itemData = new Dictionary<string, ItemData>();
    public delegate void OnComplete();
    public OnComplete OnLevelCompleted;

    public void Initialaze()
    {
        _gameItems = new List <Item>(GetComponentsInChildren<Item>()); // находим все объекты Item
        foreach (var i in _gameItems) // проходимся по листу и заполняем делигат онфаид ссылками на каждый элемент
        {
            i.Initialaze();
            i.OnFind += OnFindAction;
            
            if(!_itemData.ContainsKey(i.Name))
            {
                _itemData.Add(i.Name, new ItemData(i.ItemSprite, 1));
            }
            else
            {
                _itemData[i.Name].Amount++;
            }
        }
    }
     
    public Dictionary<string, ItemData> GetItemData()
    {
        return _itemData;
    }

    private void OnFindAction(Item item) 
    {
        _gameItems.Remove(item); // удалаем когда произошло событие нахождения
        item.OnFind -= OnFindAction; //удаляет ссылку на этот объект
        if (_gameItems.Count==0)
        {
            OnLevelCompleted?.Invoke();
            Debug.Log("Вы нашли все обекты");
        }
    }
    
}


//private void Update()
//{
//   int activeCount = 0;
//   foreach( var i in _gameItems)
//    {
//        if (i.gameObject.activeSelf)
//        {
//            activeCount++;
//        }
//    }
//   if (activeCount == 0)
//    {
//        Debug.Log("Вы нашли все обекты");
//    }