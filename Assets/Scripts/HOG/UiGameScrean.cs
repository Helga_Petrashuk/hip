﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiGameScrean : MonoBehaviour
{
    [SerializeField] private RectTransform _content = null;
    [SerializeField] private GameObject _itemPrefab = null;

    public void Initialize (Level level)
    {
        Dictionary<string, ItemData> _items = level.GetItemData();

        foreach(string key in _items.Keys)
        {
            GameObject newItem = Instantiate(_itemPrefab, _content);
            newItem.GetComponent<UIItemController>().Initialaze(_items[key].IremSprite, _items[key].Amount);
        }
    }
}
