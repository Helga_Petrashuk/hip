﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;

public class GameManager : MonoBehaviour
{
    [SerializeField] private List<Level> _levels;
    [SerializeField] private UiGameScrean _gameScrean = null;
    [SerializeField] private GameObject _endScrean = null;
    [SerializeField] private GameObject _mainScrean = null;
    
    [Header("Referencses")]
    [SerializeField] private AdsManager _adsManager = null;

    private GameObject _lavelObject = null;

    public void Awake()
    {
        _adsManager.Initialaze();
    }

    public void Start()
    {
        GameAnalytics.Initialize();
        _mainScrean.SetActive(true);
    }
    public void StartGame()
    {
        _endScrean.SetActive(false);
        _mainScrean.SetActive(false);
        _gameScrean.gameObject.SetActive(true);
        int index = Random.Range(0, _levels.Count);
        InstantiateLavel(index);
        
    }
    private void InstantiateLavel( int index)
    {
        if (_lavelObject !=null)
        {
            Destroy(_lavelObject);
            _lavelObject = null;
        }
        if (index >= _levels.Count)
        {
            return;
        }
        _lavelObject = Instantiate(_levels[index].gameObject, transform);
        Level level = _lavelObject.GetComponent<Level>();
        level.Initialaze();
        _gameScrean.Initialize(level);
        level.OnLevelCompleted += OnLevelComplete;
    }

    private void OnLevelComplete()
    {
        _adsManager.ShowInterstitial();
        _endScrean.SetActive(true);
        _lavelObject.SetActive(false);
        _gameScrean.gameObject.SetActive(false);
        
    }
}


