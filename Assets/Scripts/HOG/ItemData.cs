﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemData
{
    private Sprite _sprite = null;
    private int _amount = 0;

    public ItemData (Sprite sprite, int amount) 
    {
        _sprite = sprite;
        _amount = amount;
    }
    public Sprite IremSprite
    {
        get
        {
            return _sprite;
        }
        set
        {
            _sprite = value;
        }
    }

    public int Amount
    {
        get
        {
            return _amount;
        }
        set
        {
            if (value <0)
            {
                _amount =0;
            }
            else
            {
                _amount = value;
            }
        }
    }
}
