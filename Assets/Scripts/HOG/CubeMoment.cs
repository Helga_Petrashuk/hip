﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMoment : MonoBehaviour
{

    [SerializeField] private float _speed = 0f;
    [SerializeField] private GameObject _secondCube = null;
    [SerializeField] private GameObject _firstCube = null;
    private Vector3 _startPosizion;
    private Vector3 _direction;
    private Vector3 _targetPosizion;
    private Vector3 _testPosizion;
    private bool logic = false;
    private void Start()
    {
        _targetPosizion = _secondCube.transform.position;
        _startPosizion = _firstCube.transform.position;
        _direction = _targetPosizion - _startPosizion;
        _testPosizion = _targetPosizion;
        _direction.Normalize();
    }

    void Update()
    {
        float distance = Vector3.Distance(transform.position, _targetPosizion);
        if (logic == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, _testPosizion, (_speed * Time.deltaTime));
            if (transform.position == _targetPosizion)
            {
                _testPosizion = _startPosizion;
                logic = false;
            }
            else
            {
                if (transform.position == _startPosizion)
                {
                    _testPosizion = _targetPosizion;
                    logic = false;
                }
            }

        }

    }
    private void OnMouseDown()
    {
        logic = true;
    }
}
