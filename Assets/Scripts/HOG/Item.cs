﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Item : MonoBehaviour
{
    [SerializeField] private string _name = string.Empty;

    [Header("Settings")]
    [SerializeField] private float _scaleMultiplay;
    [SerializeField] private float _scalingTime;
    [SerializeField] private float _disapearTime;

    private SpriteRenderer _spriteRenderer;

    public string Name
    {
        get 
        {
            return _name;
        }
        private set 
        {
            _name = value;
        }
    }
    public Sprite ItemSprite => _spriteRenderer.sprite;

    public System.Action<Item> OnFind;
    
    public void Initialaze()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnMouseDown()
    {
        FindAction();
    }

    private Coroutine _corutine = null;

    private void FindAction()
    {
        /*5Vector3 newScale = transform.localScale * _scaleMultiplay;
        transform.DOScale(newScale, _scalingTime).OnComplete(() =>
        {

            _spriteRenderer.DOFade(0f, _disapearTime).OnComplete(()=>
            {
                gameObject.SetActive(false);
                OnFind?.Invoke(this);
            }
            );

        });*/

       _corutine = StartCoroutine(Scaling()); //что бы можно было остановить, StopCoroutine(_coroutine);
    }  
    

    private IEnumerator Scaling () // разобраться!!!!!
    {
        Vector3 startScale = transform.localScale;
        float currentMultiplier = 1f;
        float speed = (_scaleMultiplay - currentMultiplier) / _scalingTime;
        
        while (currentMultiplier < _scaleMultiplay)
        {

            Vector3 currentScale = startScale * currentMultiplier;
            currentMultiplier += speed * Time.deltaTime;
            transform.localScale = currentScale;
            yield return null;
        }

        yield return new WaitForSeconds(0.2f);
        float currentAlpfa = 1f;
        float alpfaSpeed = 1f / _disapearTime;
        
        while (currentAlpfa >0f)
        {
            Color color = _spriteRenderer.color;
            color.a = currentAlpfa;
            currentAlpfa -= alpfaSpeed * Time.deltaTime;
            _spriteRenderer.color = color;
            yield return null;
        }
        gameObject.SetActive(false);
        OnFind?.Invoke(this);
    }
   
}
