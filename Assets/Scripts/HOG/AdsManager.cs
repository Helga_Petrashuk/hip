﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdsManager : MonoBehaviour
{

    [SerializeField] private string _androidKey = "85460dcd";

    public void Initialaze()
    {
        IronSource.Agent.init(_androidKey);
        IronSource.Agent.validateIntegration();
        IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.BOTTOM);
        IronSource.Agent.loadInterstitial();
    }

    private void OnEnable()
    {
        IronSourceEvents.onInterstitialAdReadyEvent += OnInterstitialLoaded;
        IronSourceEvents.onInterstitialAdLoadFailedEvent += OnLoadIterstitialFailed;
        IronSourceEvents.onInterstitialAdShowSucceededEvent += OnInterstitianShowSuccess;
        IronSourceEvents.onInterstitialAdClosedEvent += OnIntersitialClosed;
    }

    private void OnDisable()
    {

        IronSourceEvents.onInterstitialAdReadyEvent -= OnInterstitialLoaded;
        IronSourceEvents.onInterstitialAdLoadFailedEvent -= OnLoadIterstitialFailed;
        IronSourceEvents.onInterstitialAdShowSucceededEvent -= OnInterstitianShowSuccess;
        IronSourceEvents.onInterstitialAdClosedEvent -= OnIntersitialClosed;

    }

    private bool _isIntertitioalReady = false
        ;
    private void OnInterstitialLoaded()
    {
        _isIntertitioalReady = true;
    }
    private void OnIntersitialClosed()
    {
        IronSource.Agent.loadInterstitial();
    }

    private void OnLoadIterstitialFailed (IronSourceError error)
    {

        Debug.Log($"Interstitial load failed! Reason - {error.getDescription()}");
    }

    private void OnInterstitianShowSuccess()
    {
        _isIntertitioalReady = false;

    }

    public void ShowInterstitial ()
    {
        if (_isIntertitioalReady)
        {
            IronSource.Agent.showInterstitial();
        }
    }
}
