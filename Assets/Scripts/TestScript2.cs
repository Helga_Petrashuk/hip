﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript2 : MonoBehaviour
{
    [HideInInspector]public float Speed = 0f;
    //[SerializeField]private float RotatitionSpeed = 0f;

    void Awake()
    {
        Debug.Log("Awake test2");
    }
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Start Test 2");
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Speed * Time.deltaTime;
        Debug.Log("Distance:" + distance);
    }

    private void FixetUpdate()
    { 

    }

    private void OnCollisionEnter(Collision collision)
    {
        
    }

    private void OnCollisionExit(Collision collision)
    {
        
    }

}
