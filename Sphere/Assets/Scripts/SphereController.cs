﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereController : MonoBehaviour
{
    [SerializeField] private InputHandler _inputHadler = null;
    [SerializeField] private GameObject _cubePrefab = null;
    [SerializeField] private float _movementSpeed = 0f;

    private void OnEnable()
    {
        _inputHadler.OnKeyDown += OnKeyDown;
    }

    private void OnDisable()
    {
        _inputHadler.OnKeyDown -= OnKeyDown;
    }

    private void OnKeyDown(KeyCode keyCode)
    {
        switch (keyCode)
        {
            case KeyCode.UpArrow:
                Move(Vector3.up);
                break;
            case KeyCode.DownArrow:
                Move(Vector3.down);
                break;
            case KeyCode.RightArrow:
                Move(Vector3.right);
                break;
            case KeyCode.LeftArrow:
                Move(Vector3.left);
                break;
            case KeyCode.Space:
                GenerateCude();
                break;
        }
    }

    private void Move(Vector3 direction)
    {
        transform.Translate(_movementSpeed * Time.deltaTime * direction);
    }

    private void GenerateCude()
    {
        GameObject cube = Instantiate(_cubePrefab);
        cube.transform.position = transform.position;
    }
}
